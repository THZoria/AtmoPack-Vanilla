## Créer un thèmes pour sa Switch (NxThemes installer)
Tuto original de $$XAD$$#3851 (quelques modifications par Winner Nombre#0997)
Réediter pour github en .mb

__FR__ Ce tutoriel sera disponible en français et en anglais __EN__ tutorial will be available in French and English

##FR

__Tutoriel sur l'utilisation de NXThème Installer__

```
⚠️Quelques informations importantes⚠️ :
-Il n’y a aucun moyen de « bricker » votre Switch, vu que les thèmes sont seulement installés sur la carte SD.

-Quand vous changez de Firmware, downgrade ⏬  ou upgrade ⏫ 
(ex. 8.0.0->9.0.1/9.0.1->8.0.0), il est important de supprimer le thème de la carte SD, on verra comment le conserver dans la suite du tuto.
- L’extraction du menu home se fait automatiquement ! 
Sinon,vous devrez ;
    - Ouvrir NXThemes Installer
    - Cliquer sur "Extract home menu".
    - Puis, re-cliquer sur "Extract home menu".
Vous devrez faire ça à chaque downgrade ou upgrade du Firmware. 
Mais normalement, cela se fait automatiquement !
```

Pour trouver des thèmes vous pouvez allez sur [Reddit](reddit.com/r/NXThemes) ou allez sur [Themezer](themezer.net/)

__Nécessité :__ 
- Une carte SD (logique, ) 
- [NXThemes installer](https://github.com/exelix11/SwitchThemeInjector/releases/latest/download/NXThemesInstaller.nro), prendre le .nro et le mettre dans /switch/Nxthemesinstaller
- [Switch Themes Creator pour Windows](https://github.com/exelix11/SwitchThemeInjector/releases/latest/download/Release4.7.zip), prendre le “Release.zip“
- Vous inquiétez pas les utilisateurs de MacOS ou Linux, il y a un créateur en ligne, 
ou vous pouvez utilisez Wine: [/SwitchThemeInjector/v2/](https://exelix11.github.io/SwitchThemeInjector/v2/)
- Un logiciel de gestion de photos.
- Pour Windows, il y a Paint3D de préinstallé, il faut juste taper dans la barre de recherche pour le trouver . 
- Pour MacOS/Linux, utilisez [Gimp](https://www.gimp.org/downloads/), ou autre bien évidemment.

__Tutoriel__

1 - "Home menu part" : 🏠
- Cette option vous permettra de choisir l’emplacement de votre thème. <br>
Par exemple, le « Home menu » mettra le thème au menu d’accueil.

2 - "Image" : 📷

- Sélectionnez votre image.

3 - "Layout patch" : 
Ceci représente différents "design", a vous d’essayer

__Exemple de Layouts__ 

__Flow Layout__

![Flow Layout](https://user-images.githubusercontent.com/50277488/131230803-30d1d687-8f9e-42ec-b045-0ec063e02ee1.png)

JAG Layout

![JAG Layout](https://user-images.githubusercontent.com/50277488/131230826-6b9a29fc-8479-4cb5-8ebf-5b7894bb872b.png)

Careful Layout

![Careful Layout](https://user-images.githubusercontent.com/50277488/131230838-9e90d464-72dd-4538-baf9-603edf1248c2.png)

- Une fois avoir choisis les options de votre choix, cliquez sur « BUILD NXTHEMES ». 
- Mettez votre carte SD dans votre ordinateur, puis choisissez le dossier « themes » pour l’endroit de votre thème.
- Ensuite remettez votre carte dans votre switch
- Démarrez sous atmosphere, 
- Lancez NXThemes installer (si possible, sans applet mode, lancez un jeu en maintenant R), 
- Allez dans "Themes" et normalement;vous le trouverez,  
- Cliquez dessus ou appuyez sur A,
- Ensuite cliquez sur "OK", 

## EN

```
⚠️Some important information⚠️:
-There is no way to "brick" your Switch, since the themes are only installed on the SD card.

-When you change the firmware, downgrade ⏬ or upgrade ⏫ 
(ex. 8.0.0->9.0.1/9.0.1->8.0.0), it is important to remove the theme from the SD card, we will see how to keep it in the rest of the tutorial.
- The extraction of the home menu is done automatically ! 
Otherwise, you will have to ;
    - Open NXThemes Installer
    - Click on "Extract home menu".
    - Then, click again on "Extract home menu".
You will have to do this every time you downgrade or upgrade the firmware. 
But normally, it is done automatically !
```
To find themes you can go to [Reddit](reddit.com/r/NXThemes) or go to [Themezer](themezer.net/)

__Necessity :__ 
- An SD card (logical) 
- [NXThemes installer](https://github.com/exelix11/SwitchThemeInjector/releases/download/), take the .nro and put it in /switch/Nxthemesinstaller
- [Switch Themes Creator pour Windows](https://github.com/exelix11/SwitchThemeInjector/releases), take the "Release.zip" file
- Don't worry MacOS or Linux users, there is an online creator, 
or you can use Wine: [/SwitchThemeInjector/v2/](https://exelix11.github.io/SwitchThemeInjector/v2/)
- A photo management software.
- For Windows, there is Paint3D pre-installed, just type in the search bar to find it. 
- For MacOS/Linux, use [Gimp](https://www.gimp.org/downloads/), or other of course.

__Tutorial__

1 - "Home menu part" : 🏠
- This option will allow you to choose the location of your theme. <br>
For example, "Home menu" will put the theme on the home menu.

2 - "Image": 📷

- Select your image.

3 - "Layout patch": 
This represents different "design", has you try

__Example of Layouts__ 

__Flow Layout__

![Flow Layout](https://user-images.githubusercontent.com/50277488/131230803-30d1d687-8f9e-42ec-b045-0ec063e02ee1.png)

JAG Layout

![JAG Layout](https://user-images.githubusercontent.com/50277488/131230826-6b9a29fc-8479-4cb5-8ebf-5b7894bb872b.png)

Careful Layout

![Careful Layout](https://user-images.githubusercontent.com/50277488/131230838-9e90d464-72dd-4538-baf9-603edf1248c2.png)

- Once you have chosen the options of your choice, click on "BUILD NXTHEMES". 
- Put your SD card in your computer, then choose the "themes" folder for the location of your theme.
- Then put your card back in your switch
- Start under atmosphere, 
- Launch NXThemes installer (if possible, without applet mode, launch a game by holding R), 
- Go to "Themes" and normally you will find it,  
- Click on it or press A,
- Then click on "OK", 
