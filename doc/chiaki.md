
# _[ - Mini Tutorial for installing Chiaki on Switch - ]_

__EN__ tutorial will be available in French and English __FR__ Ce tutoriel sera disponible en français et en anglais

## EN

__What is Chiaki__

➡️ Chiaki is an open source application originally developed for the PC to take control of the Playstation 4

It is an alternative to Sony's remonte App. It was released on Switch and other platforms some time ago.

__To install it Chiaki__

1️⃣ : Download the latest version [Chiaki 2.1.1](https://git.sr.ht/~thestr4ng3r/chiaki/refs/download/v2.1.1/Chiaki-v2.1.1-Switch.zip)

2️⃣ : Unzip the archive, you will get a file named __Chiaki.nro__

3️⃣ : Copied the file Chiaki.nro in the switch folder of your Micro SD card

4️⃣ : Recover your PSN Account ID, to do this, use this script ➡️  : [Google Colaboratory](https://colab.research.google.com/drive/1wc4mdnJxMHQHVZLu0L_x6uJ2Yx7lf3Ra?usp=sharing)

5️⃣ : Start your console and go to the Homebrew Launcher and launch Chiaki

⚠️ If you encounter instability in Chiaki, start the Homebrew Launcher without applet mode

ℹ️ : To launch the Homebrew Launcher without the applet mode, run a game by pressing R

ℹ️ : For more advanced users you can forward either the Homebrew Lancher or Chiaki (no NSP will be provided)

6️⃣ : Configured Chiaki by getting the IP address of the console (you can check this information in the network settings of the Playstation 4)

___Warning___ PlayStation 4 and Nintendo Switch must be on the same network

7️⃣ : Put the PSN Account Devices, and on your PS4 in the tab Devices added a new device is retrieved the 8-digit code to allow the addition of the Nintendo Switch

Congratulations, you should be able to connect and play your Playstation 4 from your Nintendo Switch

## FR

__Qu'est-ce que Chiaki__

➡️ Chiaki est une application open source à la base développer sur PC permettant la prise de contrôle de la Playstation 4

C'est une alternative à la remonte App de Sony. Il est sorti sur Switch et d'autre plateformes il y a quelque temps.

__Pour l'installer Chiaki__

1️⃣ : Télécharger la dernière version [Chiaki 2.1.1](https://git.sr.ht/~thestr4ng3r/chiaki/refs/download/v2.1.1/Chiaki-v2.1.1-Switch.zip)

2️⃣ : Dézipper l'archive, vous obtiendrez un fichier nommé __Chiaki.nro__

3️⃣ : Copié le fichier Chiaki.nro dans le dossier switch de votre carte Micro SD

4️⃣ : Récupéré votre PSN Account ID, pour ce faire, utilisé ce script ➡️  : [Google Colaboratory](https://colab.research.google.com/drive/1wc4mdnJxMHQHVZLu0L_x6uJ2Yx7lf3Ra?usp=sharing)

5️⃣ : Démarrer votre console et allez dans l'Homebrew Launcher et lancer Chiaki

⚠️ Si vous rencontrez de l'instabilité dans Chiaki, démarrer l'Homebrew Launcher sans le mode applet

ℹ️ : Pour lancer l'Homebrew Launcher sans le mode applet, exécuté un jeu en restant appuyer sur R

ℹ️ : Pour les utilisateurs plus avancés vous pouvez faire un forwarder soit de l'Homebrew Lancher soit de Chiaki (aucun NSP ne vous sera fourni)

6️⃣ : Configuré Chiaki en récupérant l'adresse IP de la console (vous pouvez vérifier cette information dans les paramètres réseaux de la Playstation 4)

___warning___ Attention, la PlayStation 4 et la Nintendo Switch doivent être sur le même réseau

7️⃣ : Mettre le PSN Account Devices, et sur votre PS4 dans l'onglet Devices ajouté un nouveau périphérique est récupéré le code à 8 chiffres pour autoriser l'ajout de la Nintendo Switch

Bravo, vous devriez pouvoir vous connecter est joué à votre Playstation 4 depuis votre Nintendo Switch

![20210306_190018](https://user-images.githubusercontent.com/50277488/130319219-496aed5d-4308-4dbd-a40b-5900b1422206.png)
