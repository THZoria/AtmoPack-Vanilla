# _[ - Install Games - ]_

## EN
First of all the possible formats are .NSP; .XCI; NSZ; and XCZ (but never used). The fastest to install are the .nsp and the lightest are the .nsz (but longer to install). You must first get the game, for that magic.

Installation on PC 💻


First you will need java so install it if it's not already done [Java]( https://www.java.com/fr/). 
Then you will need the latest release of ns-usbloader : 
[📜 ns-usbloader](https://github.com/developersu/ns-usbloader/releases/latest)

(legacy for Mac, normal for Windows)

⭕ Then open the settings by pressing the gear icon on the left

✅ Allow the selection of XCI/NSZ/XCZ files for Tinfoil , then click "Download and Install drivers" to install the required drivers
Installation on console

Then you have to download this archive and extract it to the root of the SD card 

[📜 Tinwoo](https://bit.ly/34HyMpO)
Installing a game

 We will perform a first test by installing the nsp of TinWoo. Moreover, you will need it for your future installations.


[📜 TinWoo Forwarder](https://cdn.discordapp.com/attachments/660558713047875604/849721105220370492/TinWoo_0579379B12970000.nsp)
 
On your console open the image gallery and go to the TinWoo homebrew. Connect your console to your computer via a USB cable (which transfers data) and then select "Install from USB device". 
On the NS-USBloader software click on "Select .NSP files" then open the location of the TinWoo_0579379B12970000.nsp file downloaded previously. Select the files to be installed (check box) and press the bottom right icon "Send to NS". They will then appear on your Nintendo Switch screen.
Check the necessary boxes and press + on your controller. Choose the "Micro-SD card" installation location.
If your file is an .XCI a warning message from NCA signature may appear if this is the case do "I understand the risks". And finally validate the installation one last time

## FR
Tout d'abord les formats possible sont les .NSP; .XCI; NSZ; et XCZ (mais jamais utilisé). Les plus rapides à installer sont les .nsp et les plus légers sont les .nsz (mais plus long à s'installer). Vous devez tout d’abord vous procurez le jeu, pour ça magie magie.

Installation sur PC  💻


Premièrement vous aurez besoin de java donc installez le si ce n'est pas déjà fait [Java]( https://www.java.com/fr/). 
Ensuite vous allez avoir besoin de la dernière release de ns-usbloader : 
[📜 ns-usbloader](https://github.com/developersu/ns-usbloader/releases/latest)

(legacy pour Mac, normal pour Windows)

⭕ Ouvrer ensuite les paramètres en appuyant sur l'icône de l'engrenage à gauche

✅ Autoriser la sélection de fichiers XCI/NSZ/XCZ pour Tinfoil , ensuite cliquez sur "Download and Install drivers" pour installer les drivers requis
Installation sur console

Ensuite il vous faudra télécharger cette archive et l'extraire à la racine de la carte SD 

[📜 Tinwoo](https://bit.ly/34HyMpO)
Installation d'un jeu

 Nous allons effectuer un premier test en installant le nsp de TinWoo. De plus, vous en aurez besoin pour vos futures installations.


[📜 TinWoo Forwarder](https://cdn.discordapp.com/attachments/660558713047875604/849721105220370492/TinWoo_0579379B12970000.nsp)
 
Sur votre console ouvrez la galerie d'image et allez sur le homebrew TinWoo. Branchez votre console à votre ordinateur via un câble USB (qui transfert les données) puis sélectionnez "Installer à partir d'un périphérique USB". 
Sur le logiciel NS-USBloader cliquez sur "Selectionner les fichiers .NSP" ouvrez ensuite l'emplacement du fichier TinWoo_0579379B12970000.nsp téléchargé précédemment. Sélectionnez les fichiers à installer (case à cocher) et appuyez sur l'icône en bas à droite "Envoyer vers NS". Ils apparaîtront ensuite sur l'écran de votre Nintendo Switch.
Cochez les cases nécessaires et appuyez sur + sur votre manette. Choisissez l'emplacement de d'installation "Carte micro-SD".
Si votre fichier est un .XCI un message de mise en garde de signature NCA risque d'apparaître si c'est le cas faîtes "Je comprends les risques". Et enfin pour terminer validez l'installation une dernière fois
